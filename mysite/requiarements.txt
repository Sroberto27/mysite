asgiref==3.2.10
-e git+https://github.com/django/django.git@3e753d3de33469493b1f0947a2e0152c4000ed40#egg=Django
pytz==2020.1
sqlparse==0.3.1
parameterized==0.7.4