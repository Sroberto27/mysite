# mysite
## install python requirements

```bash
pip install -r requerements.text
```
## Apply migrations
```bash
python manage.py migrate
```
## run the django develop server
```bash
python manage.py runserver
```
